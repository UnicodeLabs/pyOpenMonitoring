from pyOpenRPA.Tools.RobotRDPActive import Scheduler
inConfiguration={
            "ActivityTimeCheckLoopSeconds":5, #Количество секунд, между циклами проверки действий
            "ActivityTimeList": [
                {
                    "TimeHH:MM": "22:23", #Time [HH:MM] to trigger activity
                    "WeekdayList": [1,2,3,4,5,6,7], #List of the weekday index when activity is applicable, Default [1,2,3,4,5,6,7]
                    "Activity":{
                        "ModulePath": "Session\\SessionDefs.py", # "Session\\SessionDefs.py"
                        "DefName":"test", # Function name
                        "ArgList":[1,2,3], # Args list
                        "ArgDict":{"ttt":1,"222":2,"dsd":3} # Args dictionary
                    }
                },
                {
                    "TimeHH:MM": "14:17", #Time [HH:MM] to trigger activity
                    "WeekdayList": [1, 2, 3], #List of the weekday index when activity is applicable, Default [1,2,3,4,5,6,7]
                    "Activity":{
                        "ModulePath": "Session\\SessionDefs.py", # "Session\\SessionDefs.py"
                        "DefName":"test", # Function name
                        "ArgList":[1,2,3], # Args list
                        "ArgDict":{"ttt":1,"222":2,"dsd":3} # Args dictionary
                    }
                },
                {
                    "TimeHH:MMStart": "12:40", #Time [HH:MM] to trigger activity
                    "TimeHH:MMStop": "14:36",
                    "ActivityIntervalSeconds": 2,
                    "WeekdayList": [1, 2, 3, 4, 5, 6, 7], #List of the weekday index when activity is applicable, Default [1,2,3,4,5,6,7]
                    "Activity":{
                        "ModulePath": "Session\\SessionDefs.py", # "Session\\SessionDefs.py"
                        "DefName":"test", # Function name
                        "ArgList":[1,2,3], # Args list
                        "ArgDict":{"ttt":1,"222":2,"dsd":3} # Args dictionary
                    }
                }
            ]
        }
lT = Scheduler.Scheduler(inConfiguration)
